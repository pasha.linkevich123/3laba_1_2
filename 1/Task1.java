import java.util.*;

public class Task1 {

    public static int maxDig(int n) {
        int a, m = 0;
        while (n > 0) {
            a = (int) n % 10;
            if (a > m) m = a;
            n /= 10;
        }
        return m;
    }

    public static void task1Part1() {
        int digit = rand();
        System.out.println("a - " + digit + ",maxDigit - " + maxDig(digit));
    }

    public static int rand() {
        int a;

        do {
            a = new java.util.Random().nextInt(999);
        }
        while (a <= 100);
        return a;
    }

    public static void task1Part2() {
        int digit1 = rand();
        int digit2 = rand();
        int digit3 = rand();
        System.out.println("digit1 - " + digit1 + ",digit2 - " + digit2 + ",digit3 - " + digit3 + " sum - "
                + (firstDigit(digit1) + firstDigit(digit2) + firstDigit(digit3)));
    }

    public static int firstDigit(int digit) {
        int x = 0;
        for (int i = 0; i < 3; i++) {
            x = digit % 10;
            digit /= 10;
        }
        return x;
    }

    public static void task1Part3() {
        int digit1 = rand();
        int digit2 = rand();
        int digit3 = rand();
        System.out.println("digit1 - " + digit1 + ",digit2 - " + digit2 + ",digit3 - " + digit3 + " razn - "
                + (unificationDigit(digit1, digit2) - digit3));
    }

    public static int unificationDigit(int x, int y) {
        String str;
        str = Integer.toString(x) + Integer.toString(y);
        return Integer.parseInt(str);
    }

    public static void task1Part4() {
        int digit = rand();
        System.out.println("digit - " + digit + ", sum - " + sumDigit(digit));
    }

    public static int sumDigit(int digit) {
        int x = 0;
        for (int i = 0; i < 3; i++) {
            x += digit % 10;
            digit /= 10;
        }
        return x;
    }

    public static void main(String[] args) {
        task1Part1();
        task1Part2();
        task1Part3();
        task1Part4();
    }
}
